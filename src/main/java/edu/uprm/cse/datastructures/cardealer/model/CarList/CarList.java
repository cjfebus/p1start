package edu.uprm.cse.datastructures.cardealer.model.CarList;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

public class CarList {

	private static CircularSortedDoublyLinkedList<Car> cList = new CircularSortedDoublyLinkedList<>(
			new CarComparator());

	private CarList() {
	}

	public static CircularSortedDoublyLinkedList<Car> getInstance() {
		return cList;
	}

	// resets the car list to 0
	public static void resetCars() {
		cList.clear();
	}
}
