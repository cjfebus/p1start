package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList.CarList;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

@Path("/")
public class CarManager {

	private static CircularSortedDoublyLinkedList<Car> CList = CarList.getInstance();

	// Add a car
	@POST
	@Path("/cars/add")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addCar(Car c) {
		CList.add(c);

		return Response.status(201).build();
	}

	// Get a car by ID.
	@GET
	@Path("/cars/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCarByID(@PathParam("id") long id) {

		for (int i = 0; i < CList.size(); i++) {
			if (CList.get(i).getCarId() == id)
				return CList.get(i);
		}

		throw new WebApplicationException(404);
	}

	// Get all cars.
	@GET
	@Path("/cars")
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		if (CList.size() == 0)
			return new Car[1];

		Car[] result = new Car[CList.size()];

		for (int i = 0; i < result.length; i++) {
			result[i] = CList.get(i);
		}

		return result;
	}

	// Update specific car.
	@PUT
	@Path("/cars/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCarByID(@PathParam("id") long id, Car car) {

		for (int i = 0; i < CList.size(); i++) {
			if (CList.get(i).getCarId() == id) {
				CList.remove(i);
				CList.add(car);

				return Response.status(Response.Status.OK).build();
			}
		}

		// if not found
		return Response.status(Response.Status.NOT_FOUND).build();
	}

	@DELETE
	@Path("/cars/{id}/delete")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteCarByID(@PathParam("id") long id) {
		for (int i = 0; i < CList.size(); i++) {
			if (CList.get(i).getCarId() == id) {
				CList.remove(i);

				return Response.status(Response.Status.OK).build();
			}

		}
		// if not found
		return Response.status(Response.Status.NOT_FOUND).build();
	}
}