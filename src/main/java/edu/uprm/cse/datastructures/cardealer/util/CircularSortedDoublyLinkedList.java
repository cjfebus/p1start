package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {

	// Node<E> class
	private class Node<E> {
		private Node<E> prev;
		private Node<E> next;
		private E e;

		public Node(E e) {
			this.setElement(e);
		}

		public Node<E> getPrev() {
			return this.prev;
		}

		public Node<E> getNext() {
			return this.next;
		}

		public E getElement() {
			return this.e;
		}

		public void setPrev(Node<E> place) {
			this.prev = place;
		}

		public void setNext(Node<E> place) {
			this.next = place;
		}

		public void setElement(E e) {
			this.e = e;
		}

		public String toString() {
			return String.valueOf(this.e);
		}
	}

	// Iterator
	private class CircularSortedDoublyLinkedListIterator<E> implements Iterator<E> {

		private Node<E> nextNode;

		public CircularSortedDoublyLinkedListIterator() {
			this.nextNode = (Node<E>) header.getNext();
		}

		@Override
		public boolean hasNext() {
			return !this.nextNode.equals(header);
		}

		@Override
		public E next() {
			if (this.hasNext()) {
				E res = (E) this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return res;
			} else {
				throw new NoSuchElementException();
			}
		}

	}

	private Node<E> header;
	private int currentSize;
	private Comparator comparator;

	public CircularSortedDoublyLinkedList(Comparator comparator) {
		this.header = new Node(null);
		this.header.setNext(this.header);
		this.header.setPrev(this.header);
		this.currentSize = 0;
		this.comparator = comparator;
	}

	@Override
	public Iterator iterator() {
		return new CircularSortedDoublyLinkedListIterator<E>();

	}

	@Override
	public boolean add(E e) {
		Node<E> place = new Node(e);

		if (this.isEmpty()) {
			this.header.setNext(place);
			this.header.setPrev(place);
			place.setNext(this.header);
			place.setPrev(this.header);
		} else {

			int res = comparator.compare(this.header.getPrev().getElement(), e);

			// If the car we are about to add is bigger than the last one, then add it to
			// last.
			if (res <= 0) {
				Node<E> test = this.header.getPrev();

				test.setNext(place);

				this.header.setPrev(place);

				place.setPrev(test);
				place.setNext(this.header);
			}
			// If the car we are about to add is smaller than the last car, check for the
			// car that is less than the one we are about to add.
			else {
				Node<E> target = this.header.getPrev();

				while (target.getElement() != null) {

					if (comparator.compare(target.getElement(), e) <= 0)
						break;

					target = target.getPrev();
				}
				Node<E> test = target.getNext();

				test.setPrev(place);
				place.setNext(test);
				place.setPrev(target);
				target.setNext(place);
			}

		}

		this.currentSize++;

		return true;
	}

	@Override
	public int size() {
		return this.currentSize;
	}

	@Override
	public boolean remove(Object obj) {
		int i = this.firstIndex(obj);

		if (i < 0)
			return false;

		return this.remove(i);
	}

	@Override
	public boolean remove(int index) {
		if (index < 0 || index >= this.currentSize)
			throw new IndexOutOfBoundsException("Index must be > 0 or < currentSize-1");

		Node<E> target;
		int count = 0;
		for (target = this.header.getNext(); count != index; target = target.getNext(), count++)
			;

		Node<E> prev = target.getPrev();
		Node<E> next = target.getNext();

		prev.setNext(next);
		next.setPrev(prev);
		this.currentSize--;

		target = null;

		return true;
	}

	@Override
	public int removeAll(Object obj) {
		int count = 0;

		while (this.remove(obj)) {
			count++;
		}
		return count;
	}

	@Override
	public E first() {
		return (E) this.header.getNext().getElement();
	}

	@Override
	public E last() {
		return (E) this.header.getPrev().getElement();
	}

	@Override
	public E get(int index) {
		if (index < 0 || index >= this.currentSize)
			throw new IndexOutOfBoundsException();

		Node<E> target;
		int count = 0;
		for (target = this.header.getNext(); count != index; target = target.getNext(), count++)
			;

		return target.getElement();
	}

	@Override
	public void clear() {
		Node<E> target = this.header.getNext();

		while (!target.equals(this.header)) {
			this.remove(target.getElement());

			target = target.getNext();
		}
	}

	@Override
	public boolean contains(Object e) {

		Node<E> target = this.header.getNext();

		while (target.getElement() != null) {

			if (target.getElement().equals(e))
				return true;

			target = target.getNext();
		}

		return false;
	}

	@Override
	public boolean isEmpty() {
		return this.size() < 1;
	}

	@Override
	public int firstIndex(Object e) {
		int res = 0;
		Node<E> target = this.header.getNext();

		while (target.getElement() != null) {

			if (target.getElement().equals(e))
				return res;

			target = target.getNext();
			res++;
		}

		return -1;
	}

	@Override
	public int lastIndex(Object e) {
		int res = this.size() - 1;
		Node<E> target = this.header.getPrev();

		while (target.getElement() != null) {

			if (target.getElement().equals(e))
				return res;

			target = target.getPrev();
			res--;
		}

		return -1;
	}

}